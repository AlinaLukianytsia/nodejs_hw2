const joi = require('joi');
const {JWT_SECRET} = require('../configJwtSecret');
const {User} = require('../models/userModel');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const {BadRequestError} = require('../errorClass');

module.exports.register = async (req, res) => {
  const {username, password} = req.body;
  const user = new User({
    username,
    password: await bcrypt.hash(password, 10),
  });

  try {
    await user.save();
    res.json({message: 'New user created successfully'});
  } catch {
    return res.status(400).json({message: `Such user has already exists`});
  }
};

module.exports.login = async (req, res) => {
  const {username, password} = req.body;

  if (!username) {
    return res.status(400).json({message: `Please, specify username`});
  }
  if (!password) {
    return res.status(400).json({message: `Please, specify password`});
  }

  const userFind = await User.find({username: username});
  const user = userFind[0];

  if (!user) {
    return res.status(400)
        .json({message: `User '${username}' wasn't found`});
  }

  if (!(await bcrypt.compare(password, user.password))) {
    return res.status(400).json({message: `Wrong password`});
  }

  const token = jwt.sign({username: user.username, _id: user._id}, JWT_SECRET);
  await User.updateOne({_id: user._id}, {$set: {token: token}});

  res.status(200).json({message: 'Success', jwt_token: token});
};

module.exports.validateReg = async (req, res, next) => {
  const schema = joi.object({
    username: joi.string().required(),

    password: joi.string().required(),
  });

  try {
    await schema.validateAsync(req.body);
  } catch (err) {
    throw new BadRequestError(err.message);
  }

  next();
};
