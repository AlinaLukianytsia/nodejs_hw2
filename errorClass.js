// eslint-disable-next-line require-jsdoc
class BadRequestError extends Error {
  // eslint-disable-next-line require-jsdoc
  constructor(message = 'Bad request!') {
    super(message);
    this.statusCode = 400;
    this.message = message;
  }
}

module.exports = {BadRequestError};
