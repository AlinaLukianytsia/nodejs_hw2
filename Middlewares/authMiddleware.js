const jwt = require('jsonwebtoken');
const {User} = require('../models/userModel');
const {JWT_SECRET} = require('../configJwtSecret');

module.exports.authMiddleware = async (req, res, next) => {
  const token = req.headers['authorization'].slice(4);

  if (!token) {
    return res.status(400).json({message: `No Authorization header found!`});
  }

  const decoded = jwt.verify(token, JWT_SECRET);
  const user = await User.findOne({_id: decoded._id, token: token});

  if (!user) {
    res.status(400).json({message: 'Please authenticate!'});
  }

  req.token = token;
  req.userId = decoded._id;
  next();
};

