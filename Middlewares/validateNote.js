const joi = require('joi');
const {BadRequestError} = require('../errorClass');

const validateNote = async (req, res, next) => {
  const schema = joi.object({
    text: joi.string(),
  });

  try {
    await schema.validateAsync(req.body);
  } catch (err) {
    throw new BadRequestError(err.message);
  }
  next();
};

module.exports = {validateNote};
