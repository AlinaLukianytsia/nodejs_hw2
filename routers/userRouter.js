const bcrypt = require('bcrypt');
const {Router} = require('express');
// eslint-disable-next-line new-cap
const router = Router();
const {User} = require('../models/userModel');
const {Note} = require('../models/noteModel');
const {asyncWrapper} = require('../Middlewares/wrapperAsync');
const {validatePassword} = require('../Middlewares/validateNewPasword');

router.get('/', asyncWrapper(async (req, res) => {
  const currentUser = await User.findOne({_id: req.userId});
  if (!currentUser) {
    return res.status(400)
        .json({message: `Username '${currentUser}' wasn't found`});
  }

  res.status(200).json({
    user: {
      '_id': currentUser._id,
      'username': currentUser.username,
      'createdDate': currentUser.createdDate,
    },
  });
}));

router.patch('/', asyncWrapper(validatePassword),
    asyncWrapper(async (req, res) => {
      const {oldPassword, newPassword} = req.body;
      const currentUser = await User.findOne({_id: req.userId});
      if (!currentUser) {
        return res.status(400)
            .json({message: `User '${currentUser}' wasn't found`});
      }
      if (!(await bcrypt.compare(oldPassword, currentUser.password))) {
      // eslint-disable-next-line max-len
        return res.status(400).json({message: 'Old password is wrong'});
      }
      if (newPassword === oldPassword) {
      // eslint-disable-next-line max-len
        return res.status(400).json({message: 'The new password is the same as the old one.'});
      }
      const newHashPassword = await bcrypt.hash(newPassword, 10);
      await User.updateOne({_id: currentUser._id}, {password: newHashPassword});
      res.status(200).json({message: 'Success'});
    }),
);

router.delete('/', asyncWrapper(async (req, res) => {
  const currentUser = await User.findOne({_id: req.userId});
  if (!currentUser) {
    return res.status(400)
        .json({message: `User '${currentUser}' wasn't found`});
  }
  await User.deleteOne({_id: currentUser._id});
  await Note.deleteMany({userId: currentUser._id});

  res.status(200).json({message: 'Success'});
}),
);

module.exports = router;
