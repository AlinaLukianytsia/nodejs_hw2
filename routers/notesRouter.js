const {Router} = require('express');
// eslint-disable-next-line new-cap
const router = Router();
const {User} = require('../models/userModel');
const {asyncWrapper} = require('../Middlewares/wrapperAsync');
const {validateNote} = require('../Middlewares/validateNote');
const {Note} = require('../models/noteModel');

router.get('/', asyncWrapper(async (req, res) => {
  const {skip, limit} = req.query;
  const requestOptions = {skip: parseInt(skip), limit: parseInt(limit)};
  const notes = await Note.find({userId: req.userId}, {__v: 0}, requestOptions);

  res.status(200).json({notes: notes});
}));

router.post('/', asyncWrapper(validateNote), asyncWrapper(async (req, res) => {
  const {text} = req.body;
  const user = await User.findOne({_id: req.userId});
  const userId = user._id;
  const note = new Note({userId, text});

  await note.save();
  res.status(200).json({message: 'Success'});
}));

router.get('/:id', asyncWrapper(async (req, res) => {
  const note = await Note.find({userId: req.userId, _id: req.params.id},
      {__v: 0});

  if (note.length === 0) {
    // eslint-disable-next-line max-len
    return res.status(400).json({message: 'You don\'t have notes with such id'});
  }

  res.status(200).json({note: note[0]});
}));

router.put('/:id', asyncWrapper(async (req, res) => {
  const {text} = req.body;

  if (!text.trim()) {
    return res.status(400).json({message: ('Write some text for update')});
  }

  const note = await Note.find({userId: req.userId, _id: req.params.id},
      {__v: 0});

  if (note.length === 0) {
    // eslint-disable-next-line max-len
    return res.status(400).json({message: 'You don\'t have notes with such id'});
  }

  await Note.updateOne({userId: req.userId, _id: req.params.id}, {text: text});
  res.status(200).json({message: 'Success'});
}));


router.patch('/:id', asyncWrapper(async (req, res) => {
  const note = await Note.find({userId: req.userId, _id: req.params.id},
      {__v: 0});

  if (note.length === 0) {
    // eslint-disable-next-line max-len
    return res.status(400).json({message: 'You don\'t have notes with such id'});
  }

  if (!note[0].completed) {
    await Note.updateOne({userId: req.userId, _id: req.params.id},
        {completed: true});
  } else {
    await Note.updateOne({userId: req.userId, _id: req.params.id},
        {completed: false});
  }

  res.status(200).json({message: 'Success'});
}));

router.delete('/:id', asyncWrapper(async (req, res) => {
  const note = await Note.find({userId: req.userId, _id: req.params.id},
      {__v: 0});

  if (note.length === 0) {
    // eslint-disable-next-line max-len
    return res.status(400).json({message: 'You don\'t have notes with such id'});
  }

  await Note.deleteOne({userId: req.userId, _id: req.params.id});

  res.status(200).json({message: 'Success'});
}));

module.exports = router;
