const {Router} = require('express');
// eslint-disable-next-line new-cap
const router = Router();
const {asyncWrapper} = require('../Middlewares/wrapperAsync');
const {login, register, validateReg} = require('../controllers/authController');

router.post('/register', asyncWrapper(validateReg), asyncWrapper(register));
router.post('/login', asyncWrapper(login));

module.exports = router;
