const mongoose = require('mongoose');
// eslint-disable-next-line new-cap
const userSchema = mongoose.Schema({
  username: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  createdDate: {
    type: Date,
    default: Date.now,
  },
  token: {
    type: String,
  },

}).index({username: 1}, {unique: true});


module.exports.User = mongoose.model('user', userSchema);
