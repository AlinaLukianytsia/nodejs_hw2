const mongoose = require('mongoose');
// eslint-disable-next-line new-cap
const noteSchema = mongoose.Schema({
  userId: {
    type: String,
    required: true,
  },
  completed: {
    type: Boolean,
    required: true,
    default: false,
  },
  text: {
    type: String,
  },
  createdDate: {
    type: Date,
    default: Date.now,
  },
});


module.exports.Note = mongoose.model('note', noteSchema);
