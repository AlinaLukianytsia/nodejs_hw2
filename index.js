const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const app = express();
const PORT = process.env.PORT || 8080;
const {BadRequestError} = require('./errorClass');
const {asyncWrapper} = require('./Middlewares/wrapperAsync');
const {authMiddleware} = require('./Middlewares/authMiddleware');
const authRouter = require('./routers/authRouter');
const notesRouter = require('./routers/notesRouter');
const userRouter = require('./routers/userRouter');

app.use(express.json());
app.use(morgan('tiny'));
app.use(bodyParser.urlencoded({extended: false}));

app.use('/api/auth', authRouter);
app.use('/api/notes', asyncWrapper(authMiddleware), notesRouter);
app.use('/api/users/me', asyncWrapper(authMiddleware), userRouter);

app.use(async (err, req, res, next) => {
  if (err instanceof BadRequestError) {
    return res.status(err.statusCode).json({message: err.message});
  }

  return res.status(500).json({message: err.message});
});

const start = async () => {
  await mongoose.connect('mongodb+srv://testAdmin:melmang2@cluster0.wgf7k.mongodb.net/NODEJS_HW2?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  app.listen(PORT, () => {
    console.log(`Server works at port ${PORT}`);
  });
};

start();
